# Kubernetes

## Kubernetesとは？
### Kubernetes (K8s)は、デプロイやスケーリングを自動化したり、コンテナ化されたアプリケーションを管理したりするための、オープンソースのシステムです。

## なぜ、Kubernetes?
### Kubernetesを使用すると、コンテナ化されたアプリケーションをいつでもどこでも好きなときに実行できるようになり、
### それらが機能するために必要なリソースとツールを見つけやすくなります。

## Kubernetesを勉強するためには、重要な概念。
- (1)クラスター 
- クラスターは、計算、保存、およびネットワークソースの集合であり、Kubernetesはこれらのソースを使用して、コンテナーのさまざまなアプリケーションを実行します。
##
- (2)マスター 
- マスターはクラスターの大規模なものであり、その主な理由は調整です。
- つまり、Linuxオペレーティングシステムを実行し、物理マシンまたは仮想マシンのいずれかを実行します。
##
- (3)ノード
- マスター管理により、ノードはコンテナーの状態を制御および報告し、マスターの要求管理コンテナーの寿命に応じて動作します。
##
- (4)ポッド 
- ポッドは、Kubernetesの最小動作単位です。ポッド内のコンテナーは、1つのノードとして動作する1つの全体としてマスターとして機能します。
##
- (5)コントローラー
- コントローラーを介してPodを管理することにより、Podの展開特性を定義します。
- Deployment、ReplicaSet、DaemonSet、StatefuleSet、Jobなど、さまざまなコントローラーを提供します。
##
- (6)Deployment
- 最も一般的なコントローラーです。
- Deploymentを構築することにより、アプリケーションをdeployすることを保証します。
##
- (7)サービス
- 複数のサブポッドを展開できます。各ポッドには自己のIPがあります。
##
- (8)名前空間
- 名前空間は、1つの物理クラスターを複数の仮想クラスターに分割することができ、各クラスターは1つの名前空間になります。

# Kubernetesの基本操作
## クラスタを作成する(cli)
    minikube start

## アプリケーションのdeploy

    kubectl run kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080

## deployの確認
    kubectl get deployments

## Servecesの作成

    kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080

## servicesの確認
    kubectl get services
## 既存のPODを確認
    kubectl get pods
## 実行中のポッドがない場合は、ポッドを再度リストします。
    kubectl describe pods








